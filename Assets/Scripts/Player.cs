﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Player Stats")]
    [SerializeField] private float _speed = 0.5f;

    [Header("Projectile Prefabs")]
    [SerializeField] private GameObject _laser;

    [Header("Shooting Stats")] 
    [SerializeField] private float _fireRate = 0.5f;
    private float _canFire = -1f;
    [SerializeField] private int _lives = 3;
    private SpawnManager _spawnManager;

    void Start()
    {
        transform.position = new Vector3(0, 0, 0);
        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();

        if(_spawnManager == null)
        {
            Debug.Log("The Spawn Manager is NULL!");
        }
    }

    void Update()
    {
        CalculateMovement();
        
        if(Input.GetKeyDown(KeyCode.Space) && Time.time > _canFire)
        {
            FireLaser();
        }


    }

    void CalculateMovement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horizontalInput, verticalInput, 0);
        transform.Translate(direction * _speed * Time.deltaTime);

        if (transform.position.x <= -9.4)
        {
            transform.position = new Vector3(9.4f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x >= 9.4f)
        {
            transform.position = new Vector3(-9.4f, transform.position.y, transform.position.z);
        }

        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y,-4,6), 0);
    }
    void FireLaser()
    {
            _canFire = Time.time + _fireRate;
            Instantiate(_laser, transform.position + new Vector3(0, 0.8f, 0), Quaternion.identity);
    }

    public void Damage()
    {
        _lives -= 1;
        Debug.Log("Live -");

        if(_lives < 1)
        {
            //communicate with spawn manager
            _spawnManager.OnPlayerDeath();
            //Let them know to stop spawning 
            Destroy(this.gameObject);
        }
    }
}
