﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _enemySpeed= 4f;

    void Start()
    {
        
    }

    void Update()
    {
        transform.Translate(Vector3.down* _enemySpeed* Time.deltaTime);

        if(transform.position.y <-5.3f)
        {
            float randomX = Random.Range(-9.5f, 9.5f);
            transform.position = new Vector3(randomX, 8, 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Hit: " + other.transform.name);

        //If other is player
        //damage the player
        //Destroy Enemy
        if(other.tag == "Player")
        {
            Player player = other.transform.GetComponent<Player>();

            if(player != null)
            {
                player.Damage();
                Destroy(this.gameObject);
            }
            //Damage player
        }

        if(other.tag == "Laser")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
        //If other is laser
        //Destroy laser+enemy
    }
}
